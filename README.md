## Premissas:
- Não é possível adicionar duas vezes uma chave pública para o mesmo usuário
- É possível editar nome/email do usuário, mesmo que ele esteja criptografado. Isso é pelo motivo que não se criptografa os dados no banco.
- Para não ter erros na aba de teste da collection do Postman, o banco de dados deve estar limpo. Para limpar o banco, basta reiniciar a aplicação.
- Se o usuário não adicionar a chave pública, os dados serão retornado em plain text.
- Foi utilizado openApi 3.0.1

## Opções de design da API
- Para obter os digítos únicos de um determinado usuário, basta fazer uma chamada ao endpoint de Get User By Id, como descrito na documentação (openapi)

## Ferramentas
- Para testar a criptografia, gerei chaves públicas e privadas no seguinte site: https://www.devglan.com/online-tools/rsa-encryption-decryption . O qual fornece opção para verificar os valores criptografados
- Para verificar o arquivo de openapi, usei a ferramenta https://editor.swagger.io/

## Instruções de execução:

- Configuração java
```
java version "1.8.0_201"
Java(TM) SE Runtime Environment (build 1.8.0_201-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)
```

- Se o usuário tiver como executar o comando `make`, temos as seguintes opções:
```
> make clean
> make test
> make run
```

- Para executar diretamente os comandos do Maven:
```
Para limpar: > mvn clean
Para executar o programa: > mvn package && java -jar target/single-digit-0.0.1-SNAPSHOT.jar
Para executar os testes: > mvn test
```
