package com.singledigit.singledigit.domain.user;

import com.singledigit.singledigit.domain.singledigit.DigitHolder;

import java.util.ArrayList;
import java.util.List;

public class UserFixture {

    private String name;
    private String email;
    private List<DigitHolder> digitHolders;
    private Long id;
    private String publicKey;
    private String encryptedName;
    private String encryptedEmail;

    private UserFixture() {
        this.name = "name";
        this.email = "email@gmail.com";
        this.digitHolders = new ArrayList<>();
        this.id = 1L;
        this.publicKey = "a random public key";
        this.encryptedName = "encrypted-name";
        this.encryptedEmail = "ecrypted-email";
    }

    public static UserFixture anUser() {
        return new UserFixture();
    }

    public User build() {
        return new User(id, name, email, digitHolders, publicKey, encryptedName, encryptedEmail);
    }

    public UserFixture withName(String name) {
        this.name = name;

        return this;
    }

    public UserFixture withEmail(String email) {
        this.email = email;

        return this;
    }

    public UserFixture withoutDigitHolders() {
        this.digitHolders = new ArrayList<>();

        return this;
    }

    public UserFixture withDigitHolders(List<DigitHolder> digitHolders) {
        this.digitHolders = digitHolders;

        return this;
    }

    public UserFixture withId(Long id) {
        this.id = id;

        return this;
    }

    public UserFixture withPublicKey(String publicKey) {
        this.publicKey = publicKey;

        return this;
    }

    public UserFixture withoutPublicKey() {
        this.publicKey = null;

        return this;
    }

    public UserFixture withEncryptedName(String encryptedName) {
        this.encryptedName = encryptedName;

        return this;
    }

    public UserFixture withEncryptedEmail(String email) {
        this.encryptedEmail = email;

        return this;
    }
}
