package com.singledigit.singledigit.domain.user;

import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.singledigit.singledigit.domain.user.UserFixture.anUser;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

class UserTest {

    @Test
    void createsUserWithName() {
        String expectedName = "Phillip J. Coulson";

        User user = anUser().withName(expectedName).build();

        assertThat(user.getName()).isEqualTo(expectedName);
    }

    @Test
    void createsUserWithEmail() {
        String expectedEmail = "coulson@gmail.com";

        User user = anUser().withEmail(expectedEmail).build();

        assertThat(user.getEmail()).isEqualTo(expectedEmail);
    }

    @Test
    void createsUserWithoutDigitHoldersByDefault() {
        User user = anUser().withoutDigitHolders().build();

        assertThat(user.getDigitHolders()).isEmpty();
    }

    @Test
    void createsUserWithSingleListOfDigitHolders() {
        List<DigitHolder> expectedDigitHolders = singletonList(new DigitHolder("1", 1));

        User user = anUser().withDigitHolders(expectedDigitHolders).build();

        assertThat(user.getDigitHolders())
                .isEqualTo(expectedDigitHolders);
    }

    @Test
    void createsUserWithoutIdByDefault() {
        User user = new User("name", "email");

        assertThat(user.getId()).isEmpty();
    }

    @Test
    void createsUserWithId() {
        User user = anUser().withId(2L).build();

        assertThat(user.getId()).isNotEmpty();
        assertThat(user.getId().get()).isEqualTo(2L);
    }

    @Test
    void createsUserWithPublicKey() {
        User user = anUser().withPublicKey("my-key").build();

        assertThat(user.getPublicKey()).isNotEmpty();
        assertThat(user.getPublicKey().get()).isEqualTo("my-key");
    }

    @Test
    void createsUserWithEncryptedName() {
        User user = anUser().withEncryptedName("encrypted-name").build();

        assertThat(user.getEncryptedName()).isNotEmpty();
        assertThat(user.getEncryptedName().get()).isEqualTo("encrypted-name");
    }

    @Test
    void createsUserWithEncryptedEmail() {
        User user = anUser().withEncryptedEmail("encrypted-email").build();

        assertThat(user.getEncryptedEmail()).isNotEmpty();
        assertThat(user.getEncryptedEmail().get()).isEqualTo("encrypted-email");
    }

    @Test
    void createsUserAndAddPublicKeyLater() {
        User user = anUser().withoutPublicKey().build();

        assertThat(user.getPublicKey()).isEmpty();

        assertThat(user.cloneUpdatingPublicKey("public-key")
                .getPublicKey()
                .get()).isEqualTo("public-key");
    }

    @Test
    void createsUserAndDigitHolderLater() {
        User user = anUser().withoutDigitHolders().build();

        assertThat(user.getDigitHolders()).isEmpty();

        assertThat(user.cloneAddingDigitHolder(new DigitHolder("1"))
                .getDigitHolders()
                .get(0).getNumber()).isEqualTo("1");
    }
}
