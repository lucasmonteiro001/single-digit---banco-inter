package com.singledigit.singledigit.domain.user;

import com.singledigit.singledigit.application.encryption.EncryptionService;
import com.singledigit.singledigit.application.error.BusinessErrorException;
import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import com.singledigit.singledigit.persistence.user.DigitHolderEntity;
import com.singledigit.singledigit.persistence.user.UserEntity;
import com.singledigit.singledigit.persistence.user.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.singledigit.singledigit.domain.user.UserFixture.anUser;
import static com.singledigit.singledigit.persistence.user.UserEntityFixture.anUserEntity;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @Mock
    EncryptionService encryptionService;

    @Test
    void addsUser() {
        Long id = 1L;
        String name = "name";
        String email = "email";

        ArgumentCaptor<UserEntity> userEntityArgumentCaptor = ArgumentCaptor.forClass(UserEntity.class);
        when(userRepository.save(userEntityArgumentCaptor.capture())).thenReturn(new UserEntity(id, name, email));
        UserService userService = new UserService(userRepository, encryptionService);

        User addedUser = userService.addUser(new User(name, email));

        assertThat(addedUser.getName()).isEqualTo(name);
        assertThat(addedUser.getEmail()).isEqualTo(email);
        assertThat(addedUser.getId().get()).isEqualTo(id);

        UserEntity calledUserEntity = userEntityArgumentCaptor.getValue();

        assertThat(calledUserEntity.getName()).isEqualTo(name);
        assertThat(calledUserEntity.getEmail()).isEqualTo(email);
    }

    @Test
    void throwsExceptionWhenUnableToAddUser() {
        String name = "name";
        String email = "email";

        when(userRepository.save(any(UserEntity.class)))
                .thenThrow(new NullPointerException());

        UserService userService = new UserService(userRepository, encryptionService);

        assertThatThrownBy(() -> {
            userService.addUser(new User(name, email));
        }).isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to add user");

    }

    @Test
    void findsUser() {
        Long id = 1L;
        String name = "name";
        String email = "email";

        when(userRepository.findById(eq(id))).thenReturn(of(anUserEntity()
                .withId(id)
                .withName(name)
                .withEmail(email)
                .withoutPublicKey()
                .build()));

        UserService userService = new UserService(userRepository, encryptionService);

        User user = userService.find(id);

        assertThat(user.getId().get()).isEqualTo(id);
        assertThat(user.getEmail()).isEqualTo(email);
        assertThat(user.getName()).isEqualTo(name);
    }

    @Test
    void findsUserAndEncryptNameAndEmailWhenUserHasPublicKey() {
        Long id = 1L;
        String name = "Leopold";
        String email = "email@email.com";
        String encryptedName = "1f233fS#@GGA21";
        String encryptedEmail = "@ffsG$#gw3";
        String publicKey = "public-key";

        mockEncryptedValueForNameAndEmail(name, email, encryptedName, encryptedEmail, publicKey);

        when(userRepository.findById(eq(id))).thenReturn(of(anUserEntity()
                .withId(id)
                .withName(name)
                .withEmail(email)
                .build()));

        UserService userService = new UserService(userRepository, encryptionService);

        User user = userService.find(id);

        assertThat(user.getId().get()).isEqualTo(id);
        assertThat(user.getEmail()).isEqualTo(email);
        assertThat(user.getEncryptedEmail().get()).isEqualTo(encryptedEmail);
    }

    @Test
    void findsAllUsers() {
        when(userRepository.findAll())
                .thenReturn(asList(anUserEntity().withId(1L).build(),
                        anUserEntity().withId(2L).build()));

        UserService userService = new UserService(userRepository, encryptionService);

        List<User> users = userService.findAll();

        assertThat(users).hasSize(2);
        assertThat(users.get(0).getId().get()).isEqualTo(1L);
        assertThat(users.get(1).getId().get()).isEqualTo(2L);
    }

    @Test
    void throwsExceptionWhenUnableToFindAllUsers() {
        when(userRepository.findAll())
                .thenThrow(new NullPointerException());

        UserService userService = new UserService(userRepository, encryptionService);

        assertThatThrownBy(userService::findAll)
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to find users");

    }

    @Test
    void findsAllUsersWhereSecondUserHasEncryptedNameAndEmailDueToPresenceOfPublicKey() {
        String name = "Leopold";
        String email = "email@email.com";
        String encryptedName = "1f233fS#@GGA21";
        String encryptedEmail = "@ffsG$#gw3";
        String publicKey = "public-key";

        UserEntity userEntityNotEncrypted = anUserEntity()
                .withId(1L)
                .withoutPublicKey()
                .build();

        UserEntity userEntityToBeEncrypted = anUserEntity()
                .withId(2L)
                .withName(name)
                .withEmail(email)
                .withPublicKey(publicKey)
                .build();

        mockEncryptedValueForNameAndEmail(name, email, encryptedName, encryptedEmail, publicKey);

        when(userRepository.findAll())
                .thenReturn(asList(
                        userEntityNotEncrypted,
                        userEntityToBeEncrypted));

        UserService userService = new UserService(userRepository, encryptionService);

        List<User> users = userService.findAll();

        assertThat(users).hasSize(2);

        User userNotEncrypted = users.get(0);
        assertThat(userNotEncrypted.getId().get()).isEqualTo(1L);
        assertThat(userNotEncrypted.getName()).isEqualTo(userEntityNotEncrypted.getName());
        assertThat(userNotEncrypted.getEmail()).isEqualTo(userEntityNotEncrypted.getEmail());

        User userEncrypted = users.get(1);
        assertThat(userEncrypted.getId().get()).isEqualTo(2L);
        assertThat(userEncrypted.getName()).isEqualTo(userEntityToBeEncrypted.getName());
        assertThat(userEncrypted.getEncryptedName().get()).isEqualTo(encryptedName);
        assertThat(userEncrypted.getEmail()).isEqualTo(userEntityToBeEncrypted.getEmail());
        assertThat(userEncrypted.getEncryptedEmail().get()).isEqualTo(encryptedEmail);
    }

    @Test
    void throwsUserNotFoundWhenUnableToFindUser() {
        UserService userService = new UserService(userRepository, encryptionService);

        assertThatThrownBy(() -> userService.find(15L))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("User not found");
    }

    @Test
    void returnsEmptyListWhenUnableToFindAnyUser() {
        UserService userService = new UserService(userRepository, encryptionService);

        assertThat(userService.findAll()).isEmpty();
    }

    @Test
    void editsUser() {
        Long id = 1L;
        String newName = "newName";
        String newEmail = "newEmail";
        List<DigitHolder> digitHolders = singletonList(new DigitHolder("1", 1, 1));

        DigitHolderEntity digitHolderEntity = new DigitHolderEntity();
        digitHolderEntity.n = "1";
        digitHolderEntity.k = 1;
        digitHolderEntity.value = 1;

        ArgumentCaptor<UserEntity> userEntityArgumentCaptor = ArgumentCaptor.forClass(UserEntity.class);
        when(userRepository.save(userEntityArgumentCaptor.capture()))
                .thenReturn(new UserEntity(id, newName, newEmail, null, singletonList(digitHolderEntity)));

        UserService userService = new UserService(userRepository, encryptionService);

        User updatedUser = userService.editUser(new User(id, newName, newEmail, digitHolders, null));

        assertThat(updatedUser.getName()).isEqualTo(newName);
        assertThat(updatedUser.getEmail()).isEqualTo(newEmail);
        assertThat(updatedUser.getId().get()).isEqualTo(id);
        assertThat(updatedUser.getPublicKey()).isEmpty();
        List<DigitHolder> userDigitHolders = updatedUser.getDigitHolders();
        assertThat(userDigitHolders).hasSize(1);
        assertThat(userDigitHolders.get(0).getNumber()).isEqualTo("1");
        assertThat(userDigitHolders.get(0).getNumberOfConcatenations()).isEqualTo(1);
        assertThat(userDigitHolders.get(0).getSingleDigit()).isEqualTo(1);

        UserEntity calledUserEntity = userEntityArgumentCaptor.getValue();

        assertThat(calledUserEntity.getId()).isEqualTo(id);
        assertThat(calledUserEntity.getName()).isEqualTo(newName);
        assertThat(calledUserEntity.getEmail()).isEqualTo(newEmail);
        assertThat(calledUserEntity.getDigitHolders()).hasSize(1);
        assertThat(calledUserEntity.getDigitHolders().get(0).n).isEqualTo("1");
        assertThat(calledUserEntity.getDigitHolders().get(0).k).isEqualTo(1);
        assertThat(calledUserEntity.getDigitHolders().get(0).value).isEqualTo(1);
    }

    @Test
    void throwsExceptionWhenUnableToEditUser() {

        when(userRepository.save(any(UserEntity.class)))
                .thenThrow(new NullPointerException("failed"));

        UserService userService = new UserService(userRepository, encryptionService);

        assertThatThrownBy(() -> userService.editUser(anUser().build()))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to edit user");
    }

    @Test
    void deletesUserById() {
        Long id = 1L;

        UserService userService = new UserService(userRepository, encryptionService);

        userService.deleteUser(id);

        verify(userRepository, times(1)).deleteById(id);
    }

    @Test
    void throwsExceptionWhenUnableToDeleteUser() {
        doThrow(new NullPointerException())
                .when(userRepository)
                .deleteById(anyLong());

        UserService userService = new UserService(userRepository, encryptionService);

        assertThatThrownBy(() -> userService.deleteUser(1L))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to delete user");
    }

    @Test
    void addsPublicKeyToUser() {
        Long id = 1L;
        String publicKey = "public-key";
        String name = "name";
        String email = "email@email.com";
        UserEntity userEntity = anUserEntity()
                .withId(id)
                .withoutPublicKey()
                .build();

        when(encryptionService.encrypt(anyString(), anyString()))
                .thenReturn("encrypted-string");

        when(userRepository.findById(eq(id)))
                .thenReturn(of(userEntity));

        when(userRepository.save(any(UserEntity.class)))
                .thenReturn(anUserEntity()
                        .withId(id)
                        .withName(name)
                        .withEmail(email)
                        .withPublicKey(publicKey)
                        .build());

        UserService userService = new UserService(userRepository, encryptionService);

        User userBeforePublicKey = userService.find(1L);
        assertThat(userBeforePublicKey.getPublicKey()).isEmpty();

        User userAfterPublicKey = userService.addPublicKey(id, publicKey);
        assertThat(userAfterPublicKey.getPublicKey().get()).isEqualTo(publicKey);
        assertThat(userAfterPublicKey.getId().get()).isEqualTo(id);
        assertThat(userAfterPublicKey.getName()).isNotBlank();
        assertThat(userAfterPublicKey.getEmail()).isNotBlank();
    }

    @Test
    void throwsExceptionWhenTryToAddPublicKeyToUserThatHasOne() {
        Long id = 1L;
        String publicKey = "public-key";
        UserEntity userEntity = anUserEntity()
                .withId(id)
                .withPublicKey(publicKey)
                .build();

        when(userRepository.findById(eq(id)))
                .thenReturn(of(userEntity));

        UserService userService = new UserService(userRepository, encryptionService);

        assertThatThrownBy(() -> userService.addPublicKey(id, publicKey))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("User already has public key");
    }

    @Test
    void throwsExceptionWhenUnableToAssociatePublicKeyToUser() {
        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(anUserEntity().withoutPublicKey().build()));

        UserService userService = new UserService(userRepository, encryptionService);

        assertThatThrownBy(() -> userService.addPublicKey(1L, "public-key"))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to associate public key to user");
    }

    @Test
    void addsDigitHolderToUser() {

        UserService userService = new UserService(userRepository, encryptionService);

        Long userId = 1L;
        String name = "name";
        String email = "email@gmail.com";
        DigitHolder digitHolder = new DigitHolder("2");
        Integer singleDigit = 3;

        when(userRepository.findById(eq(userId)))
                .thenReturn(Optional.of(new UserEntity(userId, name, email)));

        when(userRepository.save(any(UserEntity.class)))
                .thenReturn(new UserEntity(userId, name, email, null, singletonList(new DigitHolderEntity())));

        userService.addDigitHolderWithCalculatedSingleDigit(userId, digitHolder, singleDigit);

        ArgumentCaptor<UserEntity> userEntityCaptor = ArgumentCaptor.forClass(UserEntity.class);

        verify(userRepository, times(1)).save(userEntityCaptor.capture());

        assertThat(userEntityCaptor.getValue())
                .satisfies(userEntity -> {
                    assertThat(userEntity.getId()).isEqualTo(userId);
                    assertThat(userEntity.getName()).isEqualTo(name);
                    assertThat(userEntity.getEmail()).isEqualTo(email);

                    assertThat(userEntity.getDigitHolders()).hasSize(1);
                    assertThat(userEntity.getDigitHolders().get(0))
                            .satisfies(digitHolderEntity -> {
                                assertThat(digitHolderEntity.n).isEqualTo("2");
                                assertThat(digitHolderEntity.k).isEqualTo(1);
                                assertThat(digitHolderEntity.value).isEqualTo(3);
                            });
                });

    }

    @Test
    void throwsExceptionWhenUnableToAddDigitHolderToUser() {
        when(userRepository.findById(anyLong()))
                .thenReturn(Optional.of(anUserEntity().build()));

        when(userRepository.save(any()))
                .thenThrow(new NullPointerException());

        UserService userService = new UserService(userRepository, encryptionService);

        assertThatThrownBy(() -> userService.addDigitHolderWithCalculatedSingleDigit(1L,
                new DigitHolder("2"), 3)
        ).isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to associate single digit to user");
    }

    private void mockEncryptedValueForNameAndEmail(String name, String email, String encryptedName, String encryptedEmail, String publicKey) {
        when(encryptionService.encrypt(anyString(), anyString())).thenAnswer(invocationOnMock -> {
            String key = invocationOnMock.getArgument(0);

            assertThat(key).isEqualTo(publicKey);

            String text = invocationOnMock.getArgument(1);

            if (text.equals(email)) {
                return encryptedEmail;
            } else if (text.equals(name)) {
                return encryptedName;
            }

            throw new IllegalStateException();
        });
    }
}
