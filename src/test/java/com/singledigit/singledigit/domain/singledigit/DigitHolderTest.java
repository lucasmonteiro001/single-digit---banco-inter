package com.singledigit.singledigit.domain.singledigit;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertSame;

class DigitHolderTest {

    @Test
    void createsDigitHolderWithNumber() {
        String expectedNumber = "1";

        DigitHolder digitHolder = new DigitHolder(expectedNumber);

        assertThat(digitHolder.getNumber()).isEqualTo(expectedNumber);
    }

    @Test
    void getsDigitHolderFromSingleDigitNumber() {
        DigitHolder digitHolder = new DigitHolder("1");

        assertThat(digitHolder.getSingleDigit()).isEqualTo(1);
    }

    @Test
    void getsSingleDigitalFromFourDigitNumber() {
        DigitHolder digitHolder = new DigitHolder("9875");

        assertThat(digitHolder.getSingleDigit()).isEqualTo(2);
    }

    @Test
    void getsNumberPlusNumberWhenNumberOfConcatenationIsTwo() {
        DigitHolder digitHolder = new DigitHolder("1", 2);

        assertThat(digitHolder.getSingleDigit()).isEqualTo(2);
    }

    @Test
    void getsNumberOfConcatenations() {
        DigitHolder digitHolder = new DigitHolder("123", 2);

        assertThat(digitHolder.getNumberOfConcatenations()).isEqualTo(2);
    }

    @Test
    void getsCorrectSingleDigit() {
        DigitHolder digitHolder = new DigitHolder("9875", 4);

        assertThat(digitHolder.getSingleDigit()).isEqualTo(8);
    }

    @Test
    void getsSingleDigitFromConstructor() {
        Integer singleDigit = 5;

        DigitHolder digitHolder = new DigitHolder("1", 1, singleDigit);

        assertSame(digitHolder.getSingleDigit(), singleDigit);
    }
}
