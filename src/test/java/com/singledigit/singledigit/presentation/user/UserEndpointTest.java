package com.singledigit.singledigit.presentation.user;

import com.singledigit.singledigit.application.error.ApiError;
import com.singledigit.singledigit.application.error.BusinessErrorException;
import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import com.singledigit.singledigit.domain.user.User;
import com.singledigit.singledigit.domain.user.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static com.singledigit.singledigit.domain.user.UserFixture.anUser;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserEndpointTest {

    @Mock
    private UserService userService;

    @Test
    void shouldReturnUserResponseWhenCreateUser() {
        Long id = 1L;
        String name = "Daisy Johnson";
        String email = "daisy.johnson@gmail.com";
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.name = name;
        createUserRequest.email = email;

        when(userService.addUser(any(User.class)))
                .thenReturn(new User(id, name, email));

        UserEndpoint userEndpoint = new UserEndpoint(userService);

        ResponseEntity<UserResponse> response = userEndpoint.addUser(createUserRequest);

        assertThat(response.getStatusCodeValue()).isEqualTo(200);

        UserResponse userResponse = response.getBody();

        assertThat(userResponse.id).isEqualTo(id);
        assertThat(userResponse.name).isEqualTo(name);
        assertThat(userResponse.email).isEqualTo(email);
    }

    @Test
    void findsUserById() {
        Long id = 3L;

        DigitHolder digitHolder = new DigitHolder("1");

        when(userService.find(eq(id)))
                .thenReturn(anUser().withId(id)
                        .withDigitHolders(singletonList(digitHolder))
                        .build());

        UserEndpoint userEndpoint = new UserEndpoint(userService);
        ResponseEntity<UserResponse> response = userEndpoint.findUser(id);

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody().id).isEqualTo(id);
        assertThat(response.getBody().digits).hasSize(1);
        assertThat(response.getBody().digits.get(0)).satisfies(digitHolderResponse -> {
            assertThat(digitHolderResponse.n).isEqualTo("1");
            assertThat(digitHolderResponse.k).isEqualTo(1);
            assertThat(digitHolderResponse.value).isEqualTo(1);
        });
    }

    @Test
    void findsAllUsers() {
        when(userService.findAll())
                .thenReturn(asList(anUser().withId(1L).build(),
                        anUser().withId(2L).build()));
        UserEndpoint userEndpoint = new UserEndpoint(userService);

        ResponseEntity<List<UserResponse>> allUsers = userEndpoint.findAllUsers();

        assertThat(allUsers.getStatusCodeValue()).isEqualTo(200);
        assertThat(allUsers.getBody()).hasSize(2);
        assertThat(allUsers.getBody().get(0).id).isEqualTo(1L);
        assertThat(allUsers.getBody().get(1).id).isEqualTo(2L);
    }

    @Test
    void failsToEditUserThatDoesNotExist() {
        when(userService.find(anyLong()))
                .thenThrow(new BusinessErrorException(ApiError.USER_NOT_FOUND));

        UserEndpoint userEndpoint = new UserEndpoint(userService);

        EditUserRequest editUserRequest = new EditUserRequest();
        editUserRequest.name = "name";
        editUserRequest.email = "email@gmail.com";

        assertThatThrownBy(() -> userEndpoint.editUser(1L, editUserRequest))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("User not found");
    }

    @Test
    void shouldEditUser() {
        Long id = 1L;
        String newName = "Melinda may";
        String newEmail = "may@gmail.com";
        String publicKey = "public-key";
        String encryptedName = "encrypted-name";
        String encryptedEmail = "encrypted-email";
        EditUserRequest editUserRequest = new EditUserRequest();
        editUserRequest.name = newName;
        editUserRequest.email = newEmail;

        when(userService.find(eq(id)))
                .thenReturn(anUser().withPublicKey(publicKey).withoutDigitHolders().build());

        when(userService.editUser(any(User.class)))
                .thenReturn(new User(id, newName, newEmail, new ArrayList<>(), publicKey, encryptedName, encryptedEmail));

        UserEndpoint userEndpoint = new UserEndpoint(userService);

        ResponseEntity<UserResponse> response = userEndpoint.editUser(id, editUserRequest);

        assertThat(response.getStatusCodeValue()).isEqualTo(200);

        UserResponse userResponse = response.getBody();

        assertThat(userResponse.id).isEqualTo(id);
        assertThat(userResponse.name).isEqualTo(encryptedName);
        assertThat(userResponse.email).isEqualTo(encryptedEmail);
        assertThat(userResponse.publicKey).isEqualTo(publicKey);
    }

    @Test
    void shouldDeleteUser() {
        Long id = 1L;

        UserEndpoint userEndpoint = new UserEndpoint(userService);

        ResponseEntity response = userEndpoint.deleteUserById(id);

        verify(userService, times(1)).deleteUser(eq(id));

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void shouldAddPublicKeyToUser() {
        Long id = 1L;
        String publicKey = "public-key";
        String email = "email@email.com";
        String name = "name";
        String encryptedName = "encrypted-name";
        String encryptedEmail = "encrypted-email";

        UserEndpoint userEndpoint = new UserEndpoint(userService);

        when(userService.addPublicKey(id, publicKey))
                .thenReturn(anUser()
                        .withId(id)
                        .withEmail(email)
                        .withName(name)
                        .withEncryptedName(encryptedName)
                        .withEncryptedEmail(encryptedEmail)
                        .withPublicKey(publicKey)
                        .build());

        ResponseEntity<UserResponse> response = userEndpoint.addPublicKey(id, publicKey);

        assertThat(response.getStatusCodeValue()).isEqualTo(200);

        UserResponse userResponse = response.getBody();
        assertThat(userResponse.id).isEqualTo(id);
        assertThat(userResponse.publicKey).isEqualTo(publicKey);
        assertThat(userResponse.name).isEqualTo(encryptedName);
        assertThat(userResponse.email).isEqualTo(encryptedEmail);
    }
}
