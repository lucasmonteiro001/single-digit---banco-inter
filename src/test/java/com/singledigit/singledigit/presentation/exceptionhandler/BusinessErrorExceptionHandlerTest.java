package com.singledigit.singledigit.presentation.exceptionhandler;

import com.singledigit.singledigit.application.error.ApiError;
import com.singledigit.singledigit.application.error.BusinessErrorException;
import com.singledigit.singledigit.application.error.ErrorResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class BusinessErrorExceptionHandlerTest {
    @Test
    void returnsInternalErrorWithCorrectMessage() {
        ResponseEntity<ErrorResponse> errorResponse = new BusinessErrorExceptionHandler()
                .handleBusinessException(
                        new BusinessErrorException(ApiError.UNABLE_TO_ASSOCIATE_PUBLIC_KEY),
                        mock(HttpServletRequest.class));

        assertThat(errorResponse.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        ErrorResponse errorResponseBody = errorResponse.getBody();
        assertThat(errorResponseBody.errorCode).isEqualTo(2);
        assertThat(errorResponseBody.message).isEqualTo("Unable to associate public key to user");
    }
}
