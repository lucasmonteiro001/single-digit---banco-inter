package com.singledigit.singledigit.presentation.exceptionhandler;

import com.singledigit.singledigit.application.error.ErrorResponse;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.HashSet;
import java.util.Set;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class ValidationExceptionHandlerTest {
    @Test
    void returnsResponseEntityWithErrorMessageFromConstraintViolationException() {
        ValidationExceptionHandler validationExceptionHandler = new ValidationExceptionHandler();

        ConstraintViolationException exception = mockConstraintViolationException();

        ResponseEntity<ErrorResponse> actualResponseEntityFromHandler =
                validationExceptionHandler.handleConstraintValidationExceptionOnRequestValidation(exception,
                        mock(HttpServletRequest.class));

        assertThat(actualResponseEntityFromHandler.getBody().message)
                .isEqualTo("Invalid request: [email is invalid]");
    }

    @Test
    void returnsResponseEntityWithErrorMessageFromMethodArgumentNotValidException() {
        ValidationExceptionHandler validationExceptionHandler = new ValidationExceptionHandler();

        MethodArgumentNotValidException methodArgumentNotValidException = mockMethodArgumentNotValidException();

        ResponseEntity<ErrorResponse> actualResponseEntityFromHandler = validationExceptionHandler
                .handleMethodArgumentNotValidExceptionOnRequestValidation(methodArgumentNotValidException,
                        mock(HttpServletRequest.class));

        ErrorResponse body = actualResponseEntityFromHandler.getBody();
        String expectedMessage = "Invalid request: " +
                methodArgumentNotValidException.getBindingResult().getFieldErrors().stream()
                        .map(fieldError -> format("[%s %s]", fieldError.getField(), fieldError.getDefaultMessage()))
                        .collect(joining(", "));

        assertThat(body.message).isEqualTo(expectedMessage);
    }

    private ConstraintViolationException mockConstraintViolationException() {
        ConstraintViolationException exception = mock(ConstraintViolationException.class);
        Set<ConstraintViolation> constraints = new HashSet<>();
        ConstraintViolation constraint = mock(ConstraintViolation.class);
        doReturn("is invalid").when(constraint).getMessage();

        Path path = mock(Path.class);
        doReturn("email").when(path).toString();

        doReturn(path).when(constraint).getPropertyPath();
        constraints.add(constraint);
        doReturn(constraints).when(exception).getConstraintViolations();
        return exception;
    }

    private MethodArgumentNotValidException mockMethodArgumentNotValidException() {
        MethodArgumentNotValidException methodArgumentNotValidException = mock(MethodArgumentNotValidException.class);
        stubBindingResult(methodArgumentNotValidException);
        return methodArgumentNotValidException;
    }

    private void stubBindingResult(MethodArgumentNotValidException methodArgumentNotValidException) {
        BindingResult bindingResult = mock(BindingResult.class);
        when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
    }
}
