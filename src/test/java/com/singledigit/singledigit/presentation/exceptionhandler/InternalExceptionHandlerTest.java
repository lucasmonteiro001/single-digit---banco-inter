package com.singledigit.singledigit.presentation.exceptionhandler;

import com.singledigit.singledigit.application.error.ErrorResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class InternalExceptionHandlerTest {

    @Test
    void returnsInternalErrorWithCorrectMessage() {
        ResponseEntity<ErrorResponse> errorResponse = new InternalExceptionHandler()
                .handleUnexpectedException(
                        new Exception("any message"), mock(HttpServletRequest.class), mock((HttpServletResponse.class)
                        ));

        assertThat(errorResponse.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        ErrorResponse errorResponseBody = errorResponse.getBody();
        assertThat(errorResponseBody.errorCode).isEqualTo(1);
        assertThat(errorResponseBody.message).isEqualTo("It was not possible to fulfill your request, try again later.");
    }
}
