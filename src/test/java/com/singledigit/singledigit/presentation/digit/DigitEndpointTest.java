package com.singledigit.singledigit.presentation.digit;

import com.singledigit.singledigit.application.cache.CacheService;
import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import com.singledigit.singledigit.domain.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DigitEndpointTest {

    @Mock
    CacheService cacheService;

    @Mock
    UserService userService;

    private Integer fakeValueOfSecondCacheCall = 3;

    @BeforeEach
    void setUp() {
        when(cacheService.get(any(DigitHolder.class)))
                .thenReturn(null)
                .thenReturn(fakeValueOfSecondCacheCall);
    }

    @Test
    void savesToCacheOnlyWhenValueIsNotCached() {
        CalculateDigitRequest calculateDigitRequest = new CalculateDigitRequest();
        calculateDigitRequest.n = "1";
        calculateDigitRequest.k = 2;

        DigitEndpoint digitEndpoint = new DigitEndpoint(cacheService, userService);

        ResponseEntity<String> response = digitEndpoint.calculate(null, calculateDigitRequest);

        assertThat(response.getBody()).isEqualTo("2");

        verify(cacheService).add(any(DigitHolder.class), anyInt());

        ResponseEntity<String> secondCallResponse =
                digitEndpoint.calculate(null, calculateDigitRequest);

        verify(cacheService, times(1))
                .add(any(DigitHolder.class), anyInt());

        assertThat(secondCallResponse.getBody()).isEqualTo(fakeValueOfSecondCacheCall.toString());
    }

    @Test
    void shouldSaveDigitToUserIfThereIsUserIdInRequest() {
        CalculateDigitRequest calculateDigitRequest = new CalculateDigitRequest();
        calculateDigitRequest.n = "1";
        calculateDigitRequest.k = 2;

        DigitEndpoint digitEndpoint = new DigitEndpoint(cacheService, userService);

        ResponseEntity<String> response = digitEndpoint.calculate(2L, calculateDigitRequest);

        ArgumentCaptor<DigitHolder> digitHolderCaptor = ArgumentCaptor.forClass(DigitHolder.class);

        verify(userService, times(1))
                .addDigitHolderWithCalculatedSingleDigit(eq(2L), digitHolderCaptor.capture(), eq(2));

        DigitHolder capturedDigitHolder = digitHolderCaptor.getValue();

        assertThat(capturedDigitHolder.getNumber()).isEqualTo("1");
        assertThat(capturedDigitHolder.getNumberOfConcatenations()).isEqualTo(2);
        assertThat(capturedDigitHolder.getSingleDigit()).isEqualTo(2);

        assertThat(response.getBody()).isEqualTo("2");
    }
}
