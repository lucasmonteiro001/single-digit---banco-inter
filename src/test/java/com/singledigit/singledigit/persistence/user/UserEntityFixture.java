package com.singledigit.singledigit.persistence.user;

import java.util.ArrayList;

public class UserEntityFixture {

    private Long id;
    private String name;
    private String email;
    private String publicKey;

    private UserEntityFixture() {
        this.id = 5L;
        this.name = "Jemma Simmons";
        this.email = "jemma.simmons@gmail.com";
        this.publicKey = "public-key";
    }

    public static UserEntityFixture anUserEntity() {
        return new UserEntityFixture();
    }

    public UserEntityFixture withId(Long id) {
        this.id = id;

        return this;
    }

    public UserEntityFixture withName(String name) {
        this.name = name;

        return this;
    }

    public UserEntityFixture withEmail(String email) {
        this.email = email;

        return this;
    }

    public UserEntity build() {
        return new UserEntity(id, name, email, publicKey, new ArrayList<>());
    }

    public UserEntityFixture withoutPublicKey() {
        this.publicKey = null;

        return this;
    }

    public UserEntityFixture withPublicKey(String publicKey) {
        this.publicKey = publicKey;

        return this;
    }
}
