package com.singledigit.singledigit.application.encryption;

import com.singledigit.singledigit.application.error.BusinessErrorException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class EncryptionServiceTest {

    @Test
    void encryptAndDecryptText() {
        String publicKeyBase64 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAopY2BazbSfbTy1G3fbA1XoaiZOYIFhL0v80Qz3QvypV/llNOpe57/DUlY+EA0ux0ME8DAUUFHNhqtQWg5ZId7UDDxkHh1Vl/oX/ZBbSZALdVSEOHd3Fnkm1TLfwjUq2ABN/qnYMNG9TMDO7LPYzHnx7qiXxn9DY22fH3JWjEQVyEV4NRRZjSVNFK25+cBEX3t5E6/IgiyN0O+2Vhr6Asddo6XpVnwB+182xAG0u5/4RGIIgQFB5Q4ubdBvLTCIugHk83I0XwuvmQrTtI5ZUxzS/Y1VdDBK+0fWGV/yhOE6OZxjczOOcH2Q2HeBq+hznFEJZJR5DMaOOLPMwgVsQTNQIDAQAB";
        String privateKeyBase64 = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCiljYFrNtJ9tPLUbd9sDVehqJk5ggWEvS/zRDPdC/KlX+WU06l7nv8NSVj4QDS7HQwTwMBRQUc2Gq1BaDlkh3tQMPGQeHVWX+hf9kFtJkAt1VIQ4d3cWeSbVMt/CNSrYAE3+qdgw0b1MwM7ss9jMefHuqJfGf0NjbZ8fclaMRBXIRXg1FFmNJU0Urbn5wERfe3kTr8iCLI3Q77ZWGvoCx12jpelWfAH7XzbEAbS7n/hEYgiBAUHlDi5t0G8tMIi6AeTzcjRfC6+ZCtO0jllTHNL9jVV0MEr7R9YZX/KE4To5nGNzM45wfZDYd4Gr6HOcUQlklHkMxo44s8zCBWxBM1AgMBAAECggEAANnWaIsyDp4LF7pSwoakkN0Vhn+ptSveOg7BssRB58aGh2viMn4gCf7hNjecQH5aUVJoip4o27DKRi5EXE86dt5q5+O6Ydp5D4hSw3PhhR5whNNNpzNhvmNyFwCF0s3zqJLfcHmaHPYtJzgsuiK9fLi1s4iCJsYN3XAlwf1dYZYvOmEkY65s6ex8lRsFZjMFxr0OrR217J2uKiRARECpk2vo2lEUTaK3xh7FV6dEXZTzuDINbjBDX4aYSaGLNhp893Eta0ThgitOKA/v7OAM1Ku3Yl0ySARGHnTqwkt6Gao8XuIGgHS1VYfawQe3q6zuEyR9OmRc8Jeq3gmrjeL71QKBgQDefp57WSDcu7HVNmQvExZEMXLujUIDTMxb8bZwcZpoYZMNAZdYibnnr5XzVphZDy0GX5Fq7io3vHIcy+YOHPoY5+Xmd+0fgSBZHyRhBZ58re9LXs4v2pgqRCbCf6n89rDz3la4WfcyJe/K1O4UAgm6GEcITqrrBXVHsWKrybakxwKBgQC7EhZEhB3z2EOnNZMeaQjosYf4tqKkF5oxTgmxbRiNiFjijwyGGIcWCTb7GVSTzcV8GTQgHfjnTUpZEKIocuIVEnqVv0ZTDfPXv0dtnM9ewM2SoKT0Z6AFksP+oOdCFHyygVSf1FS/4IDKKGuVVDnujef650pkCKsfMXK3TrQUIwKBgQC4Y8Uhwm66oA6z/Xyviqj0YOq4Cnc/1uCx/jLNI0js4piEueHcrV9VRC4IWaaugCY3VvJj5Pc188W6WbzRy2/1PhJImvD4IAJX+bgjdA2xlFBxbSOBgy6CMLLSlAyjqSpjIMtLwrtbYxdESIz6Jeo7wKz1lISPe5mDjd2RMv1btwKBgQCq0gVLY2kGdYCxexZwOnFMKnCDCCLTvEmspBntzjURaGPh3cYN3O/di2SNPcAhh1DB1THn8faLH+S4F5w7W0xe+ZS+kP7icGctpxjOWtZxK5WMQuzd0JIrBK28IamKibKkPbzMhdMtsw9niilTHqoXz7T5Q6KGXfQEc97QzClcCQKBgC6jw83R5eQ2ZFP9QUoeDfpcE9YiZPR+w38TJsyidL1vbiCPS51mltR8T71xLNBg0LteVXI2TYusHyiGj4cRHeGlW7K8HZQ5Qezb6P50j8zZVy8Oj+6nQAAIg51Rd0sQ5V5+LrMbl1BSnjh2u41LVZLcWYCTjvsdSZ/clfKYaEVw";
        String plainText = "value to be encrypted";
        EncryptionService encryptionService = new EncryptionService();

        String actualEncryptedText = encryptionService.encrypt(publicKeyBase64, plainText);
        assertThat(actualEncryptedText).isNotEqualTo(plainText);

        String decryptedText = encryptionService.decrypt(privateKeyBase64, actualEncryptedText);
        assertThat(decryptedText).isEqualTo(plainText);
    }

    @Test
    void throwsExceptionWhenUnableToEncryptData() {
        EncryptionService encryptionService = new EncryptionService();

        assertThatThrownBy(() -> encryptionService.encrypt("wrong formatted key", "text"))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to encrypt data");
    }

    @Test
    void throwsExceptionWhenUnableToDecryptData() {
        EncryptionService encryptionService = new EncryptionService();

        assertThatThrownBy(() -> encryptionService.decrypt("wrong formatted key", "text"))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to decrypt data");
    }
}
