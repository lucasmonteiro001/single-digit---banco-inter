package com.singledigit.singledigit.application.cache;

import com.singledigit.singledigit.application.error.BusinessErrorException;
import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class CacheServiceTest {

    @Test
    void retrievesCachedValue() {
        DigitHolder digitHolder = new DigitHolder("1", 2);
        Integer singleDigit = digitHolder.getSingleDigit();
        CacheService cacheService = new CacheService();

        assertThat(cacheService.get(digitHolder)).isNull();

        cacheService.add(digitHolder, singleDigit);

        assertThat(cacheService.get(digitHolder)).isEqualTo(singleDigit);
    }

    @Test
    void throwsExceptionWhenUnableToAddDigitToCache() {
        CacheService cacheService = new CacheService(null);

        assertThatThrownBy(() -> cacheService.add(new DigitHolder("1"), 1))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to add single digit to cache");
    }

    @Test
    void throwsExceptionWhenUnableToGetDigitToCache() {
        CacheService cacheService = new CacheService(null);

        assertThatThrownBy(() -> cacheService.get(new DigitHolder("1")))
                .isInstanceOf(BusinessErrorException.class)
                .hasMessage("Unable to get single digit from cache");
    }
}
