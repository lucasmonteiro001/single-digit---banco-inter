package com.singledigit.singledigit.application.cache;

import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class InMemoryCacheForDigitHolderTest {
    private DigitHolder firstDigitHolder;
    private DigitHolder secondDigitHolder;
    private DigitHolder fourthDigitHolder;

    @BeforeEach
    void setUp() {
        firstDigitHolder = new DigitHolder("1", 1);
        secondDigitHolder = new DigitHolder("2", 2);
        fourthDigitHolder = new DigitHolder("3", 3);
    }

    @Test
    void addsSingleDigitToCache() {
        InMemoryCacheForDigitHolder cache = new InMemoryCacheForDigitHolder();

        DigitHolder digitHolder = new DigitHolder("1", 2);

        addToCache(cache, digitHolder);

        assertThat(cache.size()).isEqualTo(1);
        assertThat(cache.get(digitHolder)).isEqualTo(digitHolder.getSingleDigit());
    }

    @Test
    void returnsNullWhenUnableToFindDigitHolderInCache() {
        InMemoryCacheForDigitHolder cache = new InMemoryCacheForDigitHolder();

        assertThat(cache.get(firstDigitHolder)).isEqualTo(null);
    }

    @Test
    void findsCorrectSingleDigitWhenThereAreMoreThanOneDigitHolderInCache() {
        InMemoryCacheForDigitHolder cache = new InMemoryCacheForDigitHolder();

        addToCache(cache, firstDigitHolder);
        addToCache(cache, secondDigitHolder);

        assertThat(cache.size()).isEqualTo(2);
        assertThat(cache.get(firstDigitHolder)).isEqualTo(firstDigitHolder.getSingleDigit());
        assertThat(cache.get(secondDigitHolder)).isEqualTo(secondDigitHolder.getSingleDigit());
    }

    @Test
    void doesNotOversizeCacheLimit() {
        InMemoryCacheForDigitHolder cache = new InMemoryCacheForDigitHolder(1);

        addToCache(cache, firstDigitHolder);
        addToCache(cache, secondDigitHolder);

        assertThat(cache.size()).isEqualTo(1);
        assertThat(cache.get(secondDigitHolder)).isEqualTo(secondDigitHolder.getSingleDigit());
    }

    @Test
    void cacheShouldWorkLikeAQueueWithMaxLimit() {
        InMemoryCacheForDigitHolder cache = new InMemoryCacheForDigitHolder(2);

        addToCache(cache, firstDigitHolder);
        addToCache(cache, secondDigitHolder);
        assertThat(cache.size()).isEqualTo(2);

        DigitHolder firstDigitHolderCopy = new DigitHolder("1", 1);
        addToCache(cache, firstDigitHolderCopy);
        assertThat(cache.size()).isEqualTo(2);

        addToCache(cache, fourthDigitHolder);
        assertThat(cache.size()).isEqualTo(2);
        assertThat(cache.get(firstDigitHolderCopy)).isEqualTo(firstDigitHolderCopy.getSingleDigit());
        assertThat(cache.get(fourthDigitHolder)).isEqualTo(fourthDigitHolder.getSingleDigit());

        addToCache(cache, firstDigitHolder);
        assertThat(cache.size()).isEqualTo(2);
        assertThat(cache.get(firstDigitHolder)).isEqualTo(firstDigitHolder.getSingleDigit());
        assertThat(cache.get(fourthDigitHolder)).isEqualTo(fourthDigitHolder.getSingleDigit());

        addToCache(cache, secondDigitHolder);
        assertThat(cache.size()).isEqualTo(2);
        assertThat(cache.get(firstDigitHolder)).isEqualTo(firstDigitHolder.getSingleDigit());
        assertThat(cache.get(secondDigitHolder)).isEqualTo(secondDigitHolder.getSingleDigit());
    }

    private void addToCache(InMemoryCacheForDigitHolder cache, DigitHolder digitHolder) {
        cache.add(digitHolder, digitHolder.getSingleDigit());
    }
}
