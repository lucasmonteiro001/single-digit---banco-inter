package com.singledigit.singledigit.application.error;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ApiErrorTest {

    @Test
    void unexpectedErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNEXPECTED_ERROR.getCode()).isEqualTo(1);
        assertThat(ApiError.UNEXPECTED_ERROR.getDescription()).isEqualTo("Unexpected error");
    }

    @Test
    void unableToAssociatePublicKeyErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_ASSOCIATE_PUBLIC_KEY.getCode()).isEqualTo(2);
        assertThat(ApiError.UNABLE_TO_ASSOCIATE_PUBLIC_KEY.getDescription())
                .isEqualTo("Unable to associate public key to user");
    }

    @Test
    void userNotFoundErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.USER_NOT_FOUND.getCode()).isEqualTo(3);
        assertThat(ApiError.USER_NOT_FOUND.getDescription()).isEqualTo("User not found");
    }

    @Test
    void unableToAddUserErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_ADD_USER.getCode()).isEqualTo(4);
        assertThat(ApiError.UNABLE_TO_ADD_USER.getDescription()).isEqualTo("Unable to add user");
    }

    @Test
    void unableToEditUserErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_EDIT_USER.getCode()).isEqualTo(5);
        assertThat(ApiError.UNABLE_TO_EDIT_USER.getDescription()).isEqualTo("Unable to edit user");
    }

    @Test
    void unableToFindUsersErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_FIND_USERS.getCode()).isEqualTo(6);
        assertThat(ApiError.UNABLE_TO_FIND_USERS.getDescription()).isEqualTo("Unable to find users");
    }

    @Test
    void unableToAssociateDigitHolderToUserErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_ASSOCIATE_DIGIT_HOLDER_TO_USER.getCode()).isEqualTo(7);
        assertThat(ApiError.UNABLE_TO_ASSOCIATE_DIGIT_HOLDER_TO_USER.getDescription())
                .isEqualTo("Unable to associate single digit to user");
    }

    @Test
    void unableToDeleteUserErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_DELETE_USER.getCode()).isEqualTo(8);
        assertThat(ApiError.UNABLE_TO_DELETE_USER.getDescription()).isEqualTo("Unable to delete user");
    }

    @Test
    void unableToAddDigitToCacheErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_ADD_DIGIT_TO_CACHE.getCode()).isEqualTo(9);
        assertThat(ApiError.UNABLE_TO_ADD_DIGIT_TO_CACHE.getDescription())
                .isEqualTo("Unable to add single digit to cache");
    }

    @Test
    void unableToGetDigitFromCacheErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_GET_DIGIT_FROM_CACHE.getCode()).isEqualTo(10);
        assertThat(ApiError.UNABLE_TO_GET_DIGIT_FROM_CACHE.getDescription())
                .isEqualTo("Unable to get single digit from cache");
    }

    @Test
    void unableToEncryptDataErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_ENCRYPT_DATA.getCode()).isEqualTo(11);
        assertThat(ApiError.UNABLE_TO_ENCRYPT_DATA.getDescription()).isEqualTo("Unable to encrypt data");
    }

    @Test
    void unableToDecryptDataErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.UNABLE_TO_DECRYPT_DATA.getCode()).isEqualTo(12);
        assertThat(ApiError.UNABLE_TO_DECRYPT_DATA.getDescription()).isEqualTo("Unable to decrypt data");
    }

    @Test
    void invalidPayloadErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.INVALID_PAYLOAD_CODE.getCode()).isEqualTo(13);
        assertThat(ApiError.INVALID_PAYLOAD_CODE.getDescription()).isEqualTo("Invalid payload");
    }

    @Test
    void userAlreadyHasPublicKeyErrorShouldHaveCorrectNumberAndMessage() {
        assertThat(ApiError.USER_ALREADY_HAS_PUBLIC_KEY.getCode()).isEqualTo(14);
        assertThat(ApiError.USER_ALREADY_HAS_PUBLIC_KEY.getDescription()).isEqualTo("User already has public key");
    }
}
