package com.singledigit.singledigit.persistence.user;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String email;

    @ElementCollection
    @CollectionTable(
            name = "user_single_digits",
            joinColumns = @JoinColumn(name = "user_id")
    )
    @Column(name = "single_digit")
    private List<DigitHolderEntity> digitHolders;

    @Column(length = 1000)
    private String publicKey;

    protected UserEntity() {
    }

    public UserEntity(Long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public UserEntity(Long id, String name, String email, String publicKey, List<DigitHolderEntity> userDigits) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.publicKey = publicKey;
        this.digitHolders = userDigits;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public List<DigitHolderEntity> getDigitHolders() {
        return digitHolders;
    }
}
