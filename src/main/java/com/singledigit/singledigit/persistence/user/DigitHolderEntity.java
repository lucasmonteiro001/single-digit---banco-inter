package com.singledigit.singledigit.persistence.user;

import javax.persistence.Embeddable;

@Embeddable
public class DigitHolderEntity {
    public String n;
    public Integer k;
    public Integer value;
}
