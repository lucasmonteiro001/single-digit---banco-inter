package com.singledigit.singledigit.presentation.user;

import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import com.singledigit.singledigit.domain.user.User;
import com.singledigit.singledigit.domain.user.UserService;
import com.singledigit.singledigit.presentation.user.UserResponse.DigitHolderResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserEndpoint {

    private final UserService userService;

    public UserEndpoint(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<UserResponse> addUser(@Valid @RequestBody CreateUserRequest createUserRequest) {

        User userToBeAdded = convertUserRequestToDomain(createUserRequest);

        User addedUser = userService.addUser(userToBeAdded);

        return new ResponseEntity<>(convertUserFromDomainToResponse(addedUser),
                HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> findUser(@Valid @PathVariable Long id) {

        User user = userService.find(id);

        return new ResponseEntity<>(convertUserFromDomainToResponse(user),
                HttpStatus.OK);
    }

    @PostMapping("/{id}/public-key")
    public ResponseEntity<UserResponse> addPublicKey(@PathVariable Long id,
                                                     @Valid @RequestBody @NotEmpty String publicKey) {

        User user = userService.addPublicKey(id, publicKey);

        return new ResponseEntity<>(convertUserFromDomainToResponse(user), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<UserResponse>> findAllUsers() {

        List<User> users = userService.findAll();

        return new ResponseEntity<>(convertUsersFromDomainToResponse(users), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponse> editUser(@PathVariable Long id,
                                                 @RequestBody EditUserRequest editUserRequest) {
        User foundUserFromDatabase = findUserOrFail(id);

        User userToBeEdited = convertEditUserRequestToDomain(id, editUserRequest, foundUserFromDatabase);

        User updatedUser = userService.editUser(userToBeEdited);

        return new ResponseEntity<>(convertUserFromDomainToResponse(updatedUser),
                HttpStatus.OK);
    }

    private User findUserOrFail(Long id) {
        return userService.find(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserResponse> deleteUserById(@PathVariable Long id) {

        userService.deleteUser(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private List<UserResponse> convertUsersFromDomainToResponse(List<User> users) {
        return users
                .stream()
                .map(this::convertUserFromDomainToResponse)
                .collect(Collectors.toList());
    }

    private UserResponse convertUserFromDomainToResponse(User user) {
        UserResponse userResponse = new UserResponse();

        userResponse.name = user.getName();
        userResponse.email = user.getEmail();

        user.getId().ifPresent(id -> userResponse.id = id);

        user.getPublicKey().ifPresent(publicKey -> {
            userResponse.name = user.getEncryptedName().get();
            userResponse.email = user.getEncryptedEmail().get();
            userResponse.publicKey = publicKey;
        });

        if (user.getDigitHolders() != null) {
            userResponse.digits = convertDigitHoldersFromDomainToResponse(user);
        }

        return userResponse;
    }

    private List<DigitHolderResponse> convertDigitHoldersFromDomainToResponse(User user) {
        return user.getDigitHolders()
                .stream()
                .map(this::convertDigitHolderFromDomainToResponse)
                .collect(Collectors.toList());
    }

    private DigitHolderResponse convertDigitHolderFromDomainToResponse(DigitHolder digitHolder) {
        DigitHolderResponse digitHolderResponse = new DigitHolderResponse();

        digitHolderResponse.n = digitHolder.getNumber();
        digitHolderResponse.k = digitHolder.getNumberOfConcatenations();
        digitHolderResponse.value = digitHolder.getSingleDigit();

        return digitHolderResponse;
    }

    private User convertUserRequestToDomain(CreateUserRequest createUserRequest) {
        return new User(createUserRequest.name, createUserRequest.email);
    }

    private User convertEditUserRequestToDomain(Long id, EditUserRequest editUserRequest, User foundUserFromDatabase) {
        return new User(id, editUserRequest.name, editUserRequest.email,
                foundUserFromDatabase.getDigitHolders(), foundUserFromDatabase.getPublicKey().orElse(null));
    }
}
