package com.singledigit.singledigit.presentation.user;

import java.util.List;

public class UserResponse {
    public Long id;
    public String name;
    public String email;
    public String publicKey;
    public List<DigitHolderResponse> digits;

    public static class DigitHolderResponse {
        public String n;
        public Integer k;
        public Integer value;
    }
}
