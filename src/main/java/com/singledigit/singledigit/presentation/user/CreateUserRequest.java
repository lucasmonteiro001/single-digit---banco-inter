package com.singledigit.singledigit.presentation.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateUserRequest {
    @NotEmpty
    public String name;

    @NotEmpty
    @Email
    public String email;
}
