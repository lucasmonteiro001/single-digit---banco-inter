package com.singledigit.singledigit.presentation.exceptionhandler;

import com.singledigit.singledigit.application.error.ApiError;
import com.singledigit.singledigit.application.error.ErrorResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

@ControllerAdvice
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class ValidationExceptionHandler {

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ErrorResponse> handleMethodArgumentNotValidExceptionOnRequestValidation(MethodArgumentNotValidException exception, HttpServletRequest request) {

        String errorDetails = getErrorDetails(exception);

        ErrorResponse errorResponse = createErrorResponse(errorDetails);

        return ResponseEntity.status(getHttpStatusCode()).body(errorResponse);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<ErrorResponse> handleConstraintValidationExceptionOnRequestValidation(ConstraintViolationException exception, HttpServletRequest request) {

        String errorDetails = getErrorDetails(exception);

        ErrorResponse errorResponse = createErrorResponse(errorDetails);

        return ResponseEntity.status(getHttpStatusCode()).body(errorResponse);
    }

    private ErrorResponse createErrorResponse(String errorDetails) {
        return new ErrorResponse(getErrorCode(), errorDetails);
    }

    private Integer getHttpStatusCode() {
        return HttpStatus.BAD_REQUEST.value();
    }

    private Integer getErrorCode() {
        return ApiError.INVALID_PAYLOAD_CODE.getCode();
    }

    private String getErrorDetails(MethodArgumentNotValidException exception) {
        return "Invalid request: " +
                exception.getBindingResult().getFieldErrors().stream()
                        .map(fieldError -> format("[%s %s]", fieldError.getField(), fieldError.getDefaultMessage()))
                        .collect(joining(", "));
    }

    private String getErrorDetails(ConstraintViolationException exception) {
        return "Invalid request: " +
                exception.getConstraintViolations().stream()
                        .map(constraintViolation -> format("[%s %s]", getPropertyPathRemovingFirstElement(constraintViolation), constraintViolation.getMessage()))
                        .collect(joining(", "));
    }

    private String getPropertyPathRemovingFirstElement(ConstraintViolation<?> constraintViolation) {
        return constraintViolation.getPropertyPath().toString();
    }

}
