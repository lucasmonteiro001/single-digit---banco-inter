package com.singledigit.singledigit.presentation.exceptionhandler;

import com.singledigit.singledigit.application.error.ApiError;
import com.singledigit.singledigit.application.error.ErrorResponse;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
@Order
public class InternalExceptionHandler {
    private static final ApiError DEFAULT_EXCEPTION_ERROR = ApiError.UNEXPECTED_ERROR;

    @ExceptionHandler({Exception.class})
    protected ResponseEntity<ErrorResponse> handleUnexpectedException(Exception exception,
                                                                      HttpServletRequest httpServletRequest,
                                                                      HttpServletResponse response) {
        ErrorResponse errorResponse = createErrorResponse();

        return ResponseEntity.status(getHttpStatusCode()).body(errorResponse);
    }

    private Integer getHttpStatusCode() {
        return HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    private ErrorResponse createErrorResponse() {
        return new ErrorResponse(InternalExceptionHandler.DEFAULT_EXCEPTION_ERROR.getCode(),
                "It was not possible to fulfill your request, try again later.");
    }
}
