package com.singledigit.singledigit.presentation.exceptionhandler;

import com.singledigit.singledigit.application.error.BusinessErrorException;
import com.singledigit.singledigit.application.error.ErrorResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class BusinessErrorExceptionHandler {

    @ExceptionHandler({BusinessErrorException.class})
    protected ResponseEntity<ErrorResponse> handleBusinessException(BusinessErrorException exception,
                                                                    HttpServletRequest request) {
        ErrorResponse errorResponseBody = createErrorResponse(exception);

        return ResponseEntity.status(exception.getHttpStatusCode()).body(errorResponseBody);
    }

    private ErrorResponse createErrorResponse(BusinessErrorException exception) {
        return new ErrorResponse(exception.getError().getCode(), exception.getMessage());
    }
}
