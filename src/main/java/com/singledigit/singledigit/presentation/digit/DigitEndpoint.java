package com.singledigit.singledigit.presentation.digit;

import com.singledigit.singledigit.application.cache.CacheService;
import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import com.singledigit.singledigit.domain.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController("/digit/calculate")
public class DigitEndpoint {

    private CacheService cacheService;
    private UserService userService;

    public DigitEndpoint(CacheService cacheService, UserService userService) {
        this.cacheService = cacheService;
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<String> calculate(
            @RequestHeader(name = "user-id", required = false) Long userId,
            @Valid @RequestBody CalculateDigitRequest calculateDigitRequest) {

        Integer singleDigit;

        DigitHolder digitHolder = new DigitHolder(calculateDigitRequest.n, calculateDigitRequest.k);

        Integer cachedDigitHolder = cacheService.get(digitHolder);

        if (cachedDigitHolder != null) {
            singleDigit = cachedDigitHolder;
        } else {
            singleDigit = digitHolder.getSingleDigit();
            cacheService.add(digitHolder, singleDigit);
        }

        if (userId != null) {
            userService.addDigitHolderWithCalculatedSingleDigit(userId, digitHolder, singleDigit);
        }

        return new ResponseEntity<>(singleDigit.toString(), HttpStatus.OK);
    }
}
