package com.singledigit.singledigit.presentation.digit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CalculateDigitRequest {
    @Pattern(regexp = "\\d*[1-9]+\\d*", message = "value should be: 1 <= n <= 10^1000000 ")
    public String n;

    @Min(1)
    @Max(100000)
    public Integer k;
}
