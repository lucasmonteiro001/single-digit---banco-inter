package com.singledigit.singledigit.application.encryption;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

public class EncryptionWithRSA2048 implements Encryption {
    @Override
    public String encrypt(String key, String text) throws Exception {
        PublicKey publicKey = convertBase64ToPublicKey(key);

        return encryptText(text, publicKey);
    }

    @Override
    public String decrypt(String privateKeyBase64, String encryptedText) throws Exception {
        PrivateKey privateKey = convertBase64ToPrivateKey(privateKeyBase64);

        return decryptText(encryptedText, privateKey);
    }

    private PublicKey convertBase64ToPublicKey(String keyInBase64) throws NoSuchAlgorithmException, InvalidKeySpecException {
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decodeBase64String(keyInBase64));
        KeyFactory keyFactory = getRSAKeyFactory();

        return keyFactory.generatePublic(keySpec);
    }

    private PrivateKey convertBase64ToPrivateKey(String keyInBase64) throws NoSuchAlgorithmException, InvalidKeySpecException {
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decodeBase64String(keyInBase64));
        KeyFactory keyFactory = getRSAKeyFactory();

        return keyFactory.generatePrivate(keySpec);
    }

    private byte[] decodeBase64String(String keyInBase64) {
        return Base64.getDecoder().decode(keyInBase64.getBytes(UTF_8));
    }

    private KeyFactory getRSAKeyFactory() throws NoSuchAlgorithmException {
        return KeyFactory.getInstance("RSA");
    }

    private String encryptText(String text, PublicKey publicKey) throws Exception {
        Cipher encryptCipher = getRSACypher();

        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

        byte[] cipherText = encryptCipher.doFinal(text.getBytes(UTF_8));

        return Base64.getEncoder().encodeToString(cipherText);
    }

    private String decryptText(String text, PrivateKey privateKey) throws Exception {
        Cipher decryptCipher = getRSACypher();

        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);

        return new String(decryptCipher.doFinal(decodeBase64String(text)), UTF_8);
    }

    private Cipher getRSACypher() throws NoSuchAlgorithmException, NoSuchPaddingException {
        return Cipher.getInstance("RSA");
    }
}
