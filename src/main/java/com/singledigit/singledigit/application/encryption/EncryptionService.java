package com.singledigit.singledigit.application.encryption;

import com.singledigit.singledigit.application.error.ApiError;
import com.singledigit.singledigit.application.error.BusinessErrorException;
import org.springframework.stereotype.Service;

@Service
public class EncryptionService {
    public String encrypt(String publicKey, String text) {

        Encryption encryptionWithRSA2048 = new EncryptionWithRSA2048();

        try {
            return encryptionWithRSA2048.encrypt(publicKey, text);
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_ENCRYPT_DATA);
        }
    }

    public String decrypt(String privateKey, String encryptedText) {
        Encryption encryptionWithRSA2048 = new EncryptionWithRSA2048();

        try {
            return encryptionWithRSA2048.decrypt(privateKey, encryptedText);
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_DECRYPT_DATA);
        }
    }
}
