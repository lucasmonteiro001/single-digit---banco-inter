package com.singledigit.singledigit.application.encryption;

public interface Encryption {
    String encrypt(String publicKey, String text) throws Exception;

    String decrypt(String privateKey, String encryptedText) throws Exception;
}
