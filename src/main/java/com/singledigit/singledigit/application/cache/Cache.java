package com.singledigit.singledigit.application.cache;

public interface Cache<K, V> {
    void add(K key, V value);

    V get(K key);

    Integer size();
}
