package com.singledigit.singledigit.application.cache;

import com.singledigit.singledigit.application.error.ApiError;
import com.singledigit.singledigit.application.error.BusinessErrorException;
import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import org.springframework.stereotype.Service;

@Service
public class CacheService {

    private Cache<DigitHolder, Integer> cache;

    public CacheService() {
        this.cache = new InMemoryCacheForDigitHolder(2);
    }

    public CacheService(Cache<DigitHolder, Integer> cache) {
        this.cache = cache;
    }

    public void add(DigitHolder digitHolder, Integer singleDigit) {
        try {
            cache.add(digitHolder, singleDigit);
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_ADD_DIGIT_TO_CACHE);
        }
    }

    public Integer get(DigitHolder digitHolder) {
        try {
            return cache.get(digitHolder);
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_GET_DIGIT_FROM_CACHE);
        }
    }
}
