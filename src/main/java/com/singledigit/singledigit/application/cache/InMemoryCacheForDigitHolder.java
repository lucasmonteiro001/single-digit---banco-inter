package com.singledigit.singledigit.application.cache;

import com.singledigit.singledigit.domain.singledigit.DigitHolder;

import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class InMemoryCacheForDigitHolder implements Cache<DigitHolder, Integer> {

    private final ConcurrentHashMap<DigitHolder, Integer> cache;
    private final Queue<DigitHolder> cacheSizeControlQueue;
    private final Integer limit;

    public InMemoryCacheForDigitHolder() {
        this.cache = new ConcurrentHashMap<>();
        this.limit = 10;
        this.cacheSizeControlQueue = new ConcurrentLinkedQueue<>();
    }

    public InMemoryCacheForDigitHolder(Integer limit) {
        this.cache = new ConcurrentHashMap<>();
        this.limit = limit;
        this.cacheSizeControlQueue = new ConcurrentLinkedQueue<>();
    }

    @Override
    public void add(DigitHolder digitHolder, Integer singleDigitValue) {
        if (isDigitHolderAlreadyInCache(digitHolder)) {
            moveDigitHolderToTailOfQueue(digitHolder);
        } else {
            removedOldestKeyFromCacheIfLimitIsReached();

            cacheSizeControlQueue.add(digitHolder);
            cache.put(digitHolder, singleDigitValue);
        }
    }

    @Override
    public Integer get(DigitHolder key) {
        return cache.get(key);
    }

    private void moveDigitHolderToTailOfQueue(DigitHolder digitHolder) {
        cacheSizeControlQueue.remove(digitHolder);
        cacheSizeControlQueue.add(digitHolder);
    }

    private void removedOldestKeyFromCacheIfLimitIsReached() {
        if (size() >= limit) {
            DigitHolder digitHolderToBeRemoved = cacheSizeControlQueue.poll();

            if (digitHolderToBeRemoved != null) {
                cache.remove(digitHolderToBeRemoved);
            }
        }
    }

    private boolean isDigitHolderAlreadyInCache(DigitHolder digitHolder) {
        return cache.containsKey(digitHolder);
    }

    @Override
    public Integer size() {
        return cache.size();
    }
}
