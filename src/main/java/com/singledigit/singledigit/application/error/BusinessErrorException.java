package com.singledigit.singledigit.application.error;

import org.springframework.http.HttpStatus;

public class BusinessErrorException extends RuntimeException {
    private final ApiError error;

    public BusinessErrorException(ApiError error) {
        super(error.getDescription());
        this.error = error;
    }

    public ApiError getError() {
        return error;
    }

    public Integer getHttpStatusCode() {
        return HttpStatus.BAD_REQUEST.value();
    }
}
