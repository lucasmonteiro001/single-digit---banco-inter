package com.singledigit.singledigit.application.error;

public enum ApiError {

    UNEXPECTED_ERROR(1, "Unexpected error"),
    UNABLE_TO_ASSOCIATE_PUBLIC_KEY(2, "Unable to associate public key to user"),
    USER_NOT_FOUND(3, "User not found"),
    UNABLE_TO_ADD_USER(4, "Unable to add user"),
    UNABLE_TO_EDIT_USER(5, "Unable to edit user"),
    UNABLE_TO_FIND_USERS(6, "Unable to find users"),
    UNABLE_TO_ASSOCIATE_DIGIT_HOLDER_TO_USER(7, "Unable to associate single digit to user"),
    UNABLE_TO_DELETE_USER(8, "Unable to delete user"),
    UNABLE_TO_ADD_DIGIT_TO_CACHE(9, "Unable to add single digit to cache"),
    UNABLE_TO_GET_DIGIT_FROM_CACHE(10, "Unable to get single digit from cache"),
    UNABLE_TO_ENCRYPT_DATA(11, "Unable to encrypt data"),
    UNABLE_TO_DECRYPT_DATA(12, "Unable to decrypt data"),
    INVALID_PAYLOAD_CODE(13, "Invalid payload"),
    USER_ALREADY_HAS_PUBLIC_KEY(14, "User already has public key");

    private int code;
    private String description;

    ApiError(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
