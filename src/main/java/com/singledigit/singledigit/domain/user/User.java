package com.singledigit.singledigit.domain.user;

import com.singledigit.singledigit.domain.singledigit.DigitHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class User {
    private final String name;
    private final String email;
    private List<DigitHolder> digitHolders = new ArrayList<>();
    private Optional<Long> id = Optional.empty();
    private Optional<String> publicKey = Optional.empty();
    private Optional<String> encryptedEmail = Optional.empty();
    private Optional<String> encryptedName = Optional.empty();

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public User(Long id, String name, String email) {
        this.id = Optional.of(id);
        this.name = name;
        this.email = email;
    }

    public User(Long id, String name, String email, List<DigitHolder> digitHolders,
                String publicKey) {
        this.id = Optional.of(id);
        this.name = name;
        this.email = email;
        this.digitHolders = digitHolders;
        this.publicKey = Optional.ofNullable(publicKey);
    }

    public User(Long id, String name, String email, List<DigitHolder> digitHolders,
                String publicKey, String encryptedName, String encryptedEmail) {
        this.id = Optional.of(id);
        this.name = name;
        this.email = email;
        this.digitHolders = digitHolders;
        this.publicKey = Optional.ofNullable(publicKey);
        this.encryptedName = Optional.ofNullable(encryptedName);
        this.encryptedEmail = Optional.ofNullable(encryptedEmail);
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public List<DigitHolder> getDigitHolders() {
        return digitHolders;
    }

    public Optional<Long> getId() {
        return id;
    }

    public Optional<String> getEncryptedEmail() {
        return encryptedEmail;
    }

    public Optional<String> getEncryptedName() {
        return encryptedName;
    }

    public Optional<String> getPublicKey() {
        return publicKey;
    }

    public User cloneUpdatingPublicKey(String publicKey) {
        return new User(this.getId().get(), this.getName(), this.getEmail(), this.getDigitHolders(), publicKey);
    }

    public User cloneAddingDigitHolder(DigitHolder digitHolder) {
        List<DigitHolder> newDigitHolders = new ArrayList<>(this.getDigitHolders());
        newDigitHolders.add(digitHolder);

        return new User(this.getId().get(), this.getName(), this.getEmail(), newDigitHolders,
                this.publicKey.orElse(null), this.encryptedName.orElse(null), this.encryptedEmail.orElse(null));
    }
}
