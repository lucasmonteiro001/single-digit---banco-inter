package com.singledigit.singledigit.domain.user;

import com.singledigit.singledigit.application.encryption.EncryptionService;
import com.singledigit.singledigit.application.error.ApiError;
import com.singledigit.singledigit.application.error.BusinessErrorException;
import com.singledigit.singledigit.domain.singledigit.DigitHolder;
import com.singledigit.singledigit.persistence.user.DigitHolderEntity;
import com.singledigit.singledigit.persistence.user.UserEntity;
import com.singledigit.singledigit.persistence.user.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private UserRepository userRepository;
    private EncryptionService encryptionService;

    public UserService(UserRepository userRepository, EncryptionService encryptionService) {
        this.userRepository = userRepository;
        this.encryptionService = encryptionService;
    }

    public User addUser(User user) {
        try {
            UserEntity savedUserEntity = userRepository
                    .save(convertUserFromDomainToPersistence(user));

            return convertUserFromPersistenceToDomain(savedUserEntity);
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_ADD_USER);
        }

    }

    public User find(Long id) {
        try {
            Optional<UserEntity> userEntity = userRepository.findById(id);

            return convertUserFromPersistenceToDomain(userEntity.get());
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.USER_NOT_FOUND);
        }
    }

    public List<User> findAll() {
        try {

            Iterable<UserEntity> usersIterator = userRepository.findAll();

            List<User> users = new ArrayList<>();

            usersIterator
                    .forEach(userEntity -> users.add(convertUserFromPersistenceToDomain(userEntity)));

            return users;
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_FIND_USERS);
        }
    }

    public User editUser(User user) {
        try {
            UserEntity userEntity = convertUserFromDomainToPersistence(user);

            UserEntity updatedUserEntity = userRepository.save(userEntity);

            return convertUserFromPersistenceToDomain(updatedUserEntity);
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_EDIT_USER);
        }
    }

    public void deleteUser(Long id) {
        try {
            userRepository.deleteById(id);

        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_DELETE_USER);
        }
    }

    public User addPublicKey(Long id, String publicKey) {
        try {
            User user = find(id);

            failIfUserAlreadyHasPublicKey(user);

            User userWithPublicKey = user.cloneUpdatingPublicKey(publicKey);

            UserEntity updatedUserEntity = userRepository.save(convertUserFromDomainToPersistence(userWithPublicKey));

            return convertUserFromPersistenceToDomain(updatedUserEntity);
        } catch (BusinessErrorException businessErrorException) {
            throw businessErrorException;
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_ASSOCIATE_PUBLIC_KEY);
        }
    }

    private void failIfUserAlreadyHasPublicKey(User user) {
        user.getPublicKey().ifPresent((publicKey) -> {
            throw new BusinessErrorException(ApiError.USER_ALREADY_HAS_PUBLIC_KEY);
        });
    }

    public void addDigitHolderWithCalculatedSingleDigit(Long userId, DigitHolder digitHolder, Integer singleDigit) {
        try {

            User user = find(userId);

            User userWithNewDigitHolder = user.cloneAddingDigitHolder(
                    new DigitHolder(digitHolder.getNumber(), digitHolder.getNumberOfConcatenations(), singleDigit));

            editUser(userWithNewDigitHolder);
        } catch (Exception exception) {
            exception.printStackTrace();

            throw new BusinessErrorException(ApiError.UNABLE_TO_ASSOCIATE_DIGIT_HOLDER_TO_USER);
        }
    }

    private User convertUserFromPersistenceToDomain(UserEntity userEntity) {
        String name = userEntity.getName();
        String publicKey = userEntity.getPublicKey();

        if (hasPublicKey(publicKey)) {
            return new User(userEntity.getId(), name, userEntity.getEmail(),
                    convertDigitHolderFromPersistenceToDomain(userEntity.getDigitHolders()),
                    publicKey,
                    encryptionService.encrypt(publicKey, name),
                    encryptionService.encrypt(publicKey, userEntity.getEmail()));
        }

        return new User(userEntity.getId(), name, userEntity.getEmail(),
                convertDigitHolderFromPersistenceToDomain(userEntity.getDigitHolders()), publicKey);
    }

    private List<DigitHolder> convertDigitHolderFromPersistenceToDomain(List<DigitHolderEntity> singleDigits) {
        if (singleDigits == null) {
            return new ArrayList<>();
        }

        return singleDigits.stream()
                .map(digitHolderEntity -> new DigitHolder(digitHolderEntity.n, digitHolderEntity.k, digitHolderEntity.value))
                .collect(Collectors.toList());
    }

    private boolean hasPublicKey(String publicKey) {
        return publicKey != null && !publicKey.isEmpty();
    }

    private UserEntity convertUserFromDomainToPersistence(User user) {
        Long userId = user.getId().orElse(null);
        String userPublicKey = user.getPublicKey().orElse(null);

        List<DigitHolderEntity> userDigits = new ArrayList<>();

        if (!user.getDigitHolders().isEmpty()) {
            userDigits = convertDigitHoldersFromDomainToPersistence(user);
        }

        return new UserEntity(userId, user.getName(), user.getEmail(), userPublicKey, userDigits);
    }

    private List<DigitHolderEntity> convertDigitHoldersFromDomainToPersistence(User user) {
        return user.getDigitHolders()
                .stream()
                .map(this::convertDigitHolderFromDomainToPersistence)
                .collect(Collectors.toList());
    }

    private DigitHolderEntity convertDigitHolderFromDomainToPersistence(DigitHolder digitHolder) {
        DigitHolderEntity digitHolderEntity = new DigitHolderEntity();
        digitHolderEntity.n = digitHolder.getNumber();
        digitHolderEntity.k = digitHolder.getNumberOfConcatenations();
        digitHolderEntity.value = digitHolder.getSingleDigit();
        return digitHolderEntity;
    }
}
