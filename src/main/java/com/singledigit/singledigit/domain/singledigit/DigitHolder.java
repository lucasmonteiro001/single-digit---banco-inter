package com.singledigit.singledigit.domain.singledigit;

import java.util.Arrays;
import java.util.Objects;

public class DigitHolder {
    private final String number;
    private final Integer numberOfConcatenations;
    private Integer singleDigit;

    public DigitHolder(String number) {
        this.number = number;
        this.numberOfConcatenations = 1;
    }

    public DigitHolder(String number, Integer numberOfConcatenations) {
        this.number = number;
        this.numberOfConcatenations = numberOfConcatenations;
    }

    public DigitHolder(String number, Integer numberOfConcatenations, Integer singleDigit) {
        this.number = number;
        this.numberOfConcatenations = numberOfConcatenations;
        this.singleDigit = singleDigit;
    }

    public String getNumber() {
        return number;
    }

    public Integer getNumberOfConcatenations() {
        return numberOfConcatenations;
    }

    public Integer getSingleDigit() {
        if (isSingleDigitAlreadyCalculated()) {
            return this.singleDigit;
        }

        Integer calculatedSingleDigit = calculateSingleDigit(getMultipliedString());

        this.singleDigit = calculatedSingleDigit;

        return calculatedSingleDigit;
    }

    private boolean isSingleDigitAlreadyCalculated() {
        return this.singleDigit != null;
    }

    private Integer calculateSingleDigit(String numberStringMultipliedByConcatenation) {

        if (numberStringMultipliedByConcatenation.length() == 1) {
            return Integer.parseInt(numberStringMultipliedByConcatenation);
        }

        String[] numberFragment = numberStringMultipliedByConcatenation.split("");

        return calculateSingleDigit(sumStringChars(numberFragment).toString());
    }

    private Integer sumStringChars(String[] numberFragment) {
        return Arrays.stream(numberFragment)
                .mapToInt(Integer::parseInt)
                .sum();
    }

    private String getMultipliedString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int index = 0; index < numberOfConcatenations; index++) {
            stringBuilder.append(number);
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitHolder that = (DigitHolder) o;
        return number.equals(that.number) &&
                numberOfConcatenations.equals(that.numberOfConcatenations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, numberOfConcatenations);
    }
}
