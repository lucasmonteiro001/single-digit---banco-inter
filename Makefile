clean:
	mvn clean

run:
	mvn package && java -jar target/single-digit-0.0.1-SNAPSHOT.jar

test:
	mvn test
